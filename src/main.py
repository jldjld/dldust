from module import sort_files

import argparse

from module import sort_files

def main():
    # Define argument directory_path
    parser = argparse.ArgumentParser(description='Sort files in a directory.')
    parser.add_argument('directory_path', metavar='directory_path', type=str, help='Path to the directory to sort')
    args = parser.parse_args()
    if not args.directory_path:
        raise ValueError("Missing directory path.")

    sorted_files = sort_files(args.directory_path)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
