import os
import shutil

def sort_files(dir_path: str):
    exists = dir_exists(dir_path)
    if not exists:
        raise IsADirectoryError("Please verify directory path.")

    data           = list_files(dir_path)
    subdirectories = list_subdirectories(dir_path)
    if not data and not subdirectories:
        print("Directory is empty :(")
        return

    if not data and subdirectories:
        print("Directory already sorted :)")
        return
    

    file_types = {}
    for file_name in data:
        file_type = get_file_type(file_name)
        # Skip file type if it is missing
        if not file_type:
            continue
        
        if file_type not in file_types:
            file_types[file_type] = []
        file_types[file_type].append(file_name)

    for file_type in file_types:
        # Skip file type if it is missing
        if file_type not in file_types:
            continue
        
        folder_path = os.path.join(dir_path, file_type)
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
                
        for file_name in file_types[file_type]:
            file_path = os.path.join(dir_path, file_name)
            # if is installation file, delete it
            installation_type = delete_installation_files(file_type, file_path, file_name)
            if installation_type:
                continue

            new_file_path = os.path.join(folder_path, file_name)
            if os.path.exists(new_file_path):
                print("File already exists: " + file_name)
                continue

            shutil.move(file_path, new_file_path)
            print("Moved file: " + file_name)

def delete_installation_files(file_type: str, file_path: str, file_name: str):
    installation_types = ["deb", "rpm", "exe", "msi", "dmg", "pkg", "apk"]
    if file_type in installation_types:
        os.remove(file_path)
        print("Deleted installation file: " + file_name)

        return True

def list_files(path: str) -> list:
    files = []
    for file_name in os.listdir(path):
        if os.path.isfile(os.path.join(path, file_name)):
            files.append(file_name)
    return files

def get_file_type(file_name: str) -> str:
    if "." not in file_name:
        return None
    
    return file_name.split(".")[-1]


def dir_exists(path: str) -> bool:
    # Check whether the specified
    # path exists or not
    return os.path.isdir(path)


def list_subdirectories(path: str) -> list:
    subdirectories = []
    for dir_name in os.listdir(path):
        if os.path.isdir(os.path.join(path, dir_name)):
            subdirectories.append(dir_name)
            if not os.listdir(os.path.join(path, dir_name)):
                os.rmdir(os.path.join(path, dir_name))
                print("Deleted empty subdirectory: " + dir_name)

    return subdirectories